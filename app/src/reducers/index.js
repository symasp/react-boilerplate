import { combineReducers } from 'redux';
import todos from './todos';
import lang from './translation';

export default combineReducers({
    todos: todos,
    lang: lang
});