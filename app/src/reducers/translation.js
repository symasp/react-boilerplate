import {fromJS, Map} from 'immutable';

const initState = "FR";

const lang = (state = initState, action) => {
    switch (action.type) {
        case 'LANGUAGE_SWITCH':
            return state = action.lang;
        default:
            return state;
    }
};

export default lang;
