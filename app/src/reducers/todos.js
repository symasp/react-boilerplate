import {Map} from 'immutable';


const initState = new Map({
    "electron": {
        id: 'electron',
        text: 'electron',
        completed: true
    },
    "react": {
        id: 'react',
        text: 'react',
        completed: true
    },
    "webpack": {
        id: 'webpack',
        text: 'webpack',
        completed: true
    },
    "redux": {
        id: 'redux',
        text: 'redux',
        completed: true
    },
    "material": {
        id: 'material',
        text: 'material',
        completed: true
    }
});

let ID = function () {
    return '_' + Math.random().toString(36).substr(2, 9);
};

const todos = (state = initState, action) => {
    switch (action.type) {
        case 'ADD_TODO':
            let id = ID();
            const todo = {
                id: id,
                text: action.text,
                completed: false
            };
            return state = state.setIn([id], todo);
        case 'SWITCH_STATE_TODO':
            let todoState = true;
            if (state.getIn([action.id, 'completed']) === true) {
                todoState = false
            }
            return state = state.setIn([action.id, 'completed'], todoState);
        case 'DELETE_TODO':
            return state = state.deleteIn([action.id]);
        default:
            return state;
    }
};

export default todos;
