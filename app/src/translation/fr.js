const translation = {
    "general": {
        "yes": "oui",
        "no": "non"
    },
    "todo": {
        "addTodo": "Ajouter une tâche",
        "completed": "Complété : "
    }
};

export default translation;