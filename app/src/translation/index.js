import fr from './fr';
import en from './en';

const translation = {
    "FR": fr,
    "EN": en,
};

export default translation;