const translation = {
    "general": {
        "yes": "yes",
        "no": "no"
    },
    "todo": {
        "addTodo": "Add a todo",
        "completed": "Completed : "
    }
};

export default translation;