import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
    palette: {
        primary: {
            main:'#ce4c78',
            light: '#f59bba',
            dark: '#76163b'
        },
        secondary: {
            main:'#be6a4d',
            light: '#f8cfbd',
            dark: '#6d3d2f'
        },
    },
    status: {
        danger: 'orange',
    },
});

export default theme;