let nextTodoId = 0;
export const addTodo = text => ({
    type: 'ADD_TODO',
    id: nextTodoId++,
    text
});

export const deleteTodo = id => ({
    type: 'DELETE_TODO',
    id: id
});

export const switchStateTodo = id => ({
    type: 'SWITCH_STATE_TODO',
    id: id
});

export const languageSwitch = lang => ({
    type: 'LANGUAGE_SWITCH',
    lang: lang
});


