import React, {Component} from 'react';
import Home from './components/Home';

import './style.less';

class App extends Component {

    render() {
        return (
            <Home />
        )
    }
}

export default App;