import React from 'react';
import {Route} from 'react-router-dom';
import {withStyles, withTheme} from '@material-ui/core/styles';

const styles = theme => ({
    '@global': {
        '*::-webkit-scrollbar-track': {
            display: 'none'
        },

        '*::-webkit-scrollbar-thumb': {
            backgroundColor: theme.palette.primary.main,
            borderRadius: '8px'
        },

        '*::-webkit-scrollbar-thumb:hover': {
            backgroundColor: theme.palette.primary.light,
            borderRadius: '8px'
        },

        '*::-webkit-scrollbar': {
            width: '10px'
        }
    }
});

const PublicLayout = ({component: Component, ...rest}) => {
    const {path} = rest;
    return (
        <Route {...rest} render={matchProps => (
            <div>
                <Component {...matchProps} />
            </div>
        )}/>
    )
};

export default withTheme()(withStyles(styles)(PublicLayout));