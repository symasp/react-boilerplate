import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {withTheme} from '@material-ui/core/styles';
import Reactotron from 'reactotron-react-js';
import {IconButton, InputBase, List, Paper} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircle from '@material-ui/icons/AddCircle';
import translation from '../../translation/';

import Todo from '../Commons/Todo';
import LanguageSwitch from '../Commons/LanguageSwitch';

import {addTodo, switchStateTodo, deleteTodo} from '../../actions/index.js';

import './style.less';

const todoStyle = {
    backgroundColor: '#aac389',
};

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.addClick = this.addClick.bind(this);
        this.switchStateClick = this.switchStateClick.bind(this);
        this.deleteClick = this.deleteClick.bind(this);
    }

    handleChange() {
        this.setState({
            text: event.target.value,
        });
    }

    addClick() {
        const {text} = this.state;

        if (text !== null && text !== '') {
            this.props.addTodo(text);
            this.setState({
                text: ''
            });
        }
    }

    switchStateClick(id, e) {
        e.preventDefault();
        this.props.switchStateTodo(id);
    }

    deleteClick(id, e) {
        e.preventDefault();
        this.props.deleteTodo(id);
    }

    render() {
        Reactotron.warn('In state : CTRL + N & ENTER');

        const {theme, todos, lang} = this.props;

        return (
            <div>
                <div className='container'>
                    <div className='title'>
                        <h2 style={{color: theme.palette.secondary.light}}>Electron & React</h2>
                        <div className='subTitle'>
                            <div className="iconsContainer">
                                <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg"
                                     className="icon"/>
                                <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/webpack.svg"
                                     className="icon"/>
                                <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/electron.svg"
                                     className="icon"/>
                                <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg"
                                     className="icon"/>
                                <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/materialdesign.svg"
                                     className="icon"/>
                            </div>
                            <div className="languageSwitchContainer">
                                <LanguageSwitch/>
                            </div>
                            <Paper elevation={1} className="todoInput">
                                <InputBase value={this.state.text} placeholder={translation[lang].todo.addTodo}
                                           onChange={this.handleChange}/>
                                <IconButton color="primary" aria-label="Add" className="add" onClick={this.addClick}>
                                    <AddCircle/>
                                </IconButton>
                            </Paper>
                            {todos ? (
                                <div className="todoListContainer">
                                    <List dense={true}>
                                        {todos.valueSeq().map(todo => {
                                                return (
                                                    <div key={todo.id} className="todoContainer" style={todo.completed ? todoStyle : null}>
                                                        <div onClick={this.switchStateClick.bind(this, todo.id)}>
                                                            <Todo text={todo.text}
                                                                  completed={todo.completed}
                                                                  lang={lang}
                                                            />
                                                        </div>
                                                        <IconButton className="deleteIcon" aria-label="Delete"
                                                                    onClick={this.deleteClick.bind(this, todo.id)}>
                                                            <DeleteIcon/>
                                                        </IconButton>
                                                    </div>
                                                );
                                            }
                                        )}
                                    </List>
                                </div>
                            ) : (
                                <div className="todoListContainer"></div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const todos = state.todos;
    const lang = state.lang;
    return {
        todos: todos,
        lang: lang
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addTodo: (text) => dispatch(addTodo(text)),
        deleteTodo: (id) => dispatch(deleteTodo(id)),
        switchStateTodo: (id) => dispatch(switchStateTodo(id))
    }
};

Home.propTypes = {
    theme: PropTypes.object.isRequired
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTheme()(Home));