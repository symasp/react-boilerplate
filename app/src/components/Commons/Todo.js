import React, {Component} from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import PropTypes from "prop-types";
import translation from '../../translation/';

class Todo extends Component {

    render() {
        const {text, completed, lang} = this.props;
        return (
            <span>
                <ListItem button>
                    <ListItemText
                        primary={text}
                        secondary={
                            completed ?
                                translation[lang].todo.completed + translation[lang].general.yes
                                :
                                translation[lang].todo.completed + translation[lang].general.no
                        }
                    />
                </ListItem>
                <Divider/>
            </span>
        );
    }
}

Todo.propTypes = {
    text: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
};

export default Todo;