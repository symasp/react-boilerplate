import React, {Component} from 'react';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import {languageSwitch} from "../../actions";
import connect from "react-redux/es/connect/connect";
import {withTheme} from "@material-ui/core";
import translation from '../../translation/';

/*
import PropTypes from "prop-types";
import translation from '../../translation/';
*/

class LanguageSwitch extends Component {

    constructor(props) {
        super(props);

        this.state = {
            language: 'FR'
        };

        this.handleLanguage = this.handleLanguage.bind(this);
    }

    handleLanguage(event, language) {
        this.setState({
            language
        });
        this.props.languageSwitch(language);
    }

    render() {
        const { lang } = this.props;
        const {language} = this.state;

        return (
            <div>
                <ToggleButtonGroup value={language.toString()} exclusive onChange={this.handleLanguage}>
                    <ToggleButton value="FR" disabled={lang === "FR"}>
                        FR
                    </ToggleButton>
                    <ToggleButton value="EN" disabled={lang === "EN"}>
                        EN
                    </ToggleButton>
                </ToggleButtonGroup>
            </div>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const lang = state.lang;
    return {
        lang: lang
    }
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        languageSwitch: (lang) => dispatch(languageSwitch(lang)),
    }
};


LanguageSwitch.propTypes = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withTheme()(LanguageSwitch));