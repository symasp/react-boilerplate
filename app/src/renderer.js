import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, compose} from 'redux';
import Reactotron from './ReactotronConfig';
import rootReducer from './reducers/';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import Layout from './Components/Layouts/PublicLayout'
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import App from './App.js';
import './ReactotronConfig';

const store = createStore(rootReducer, compose(Reactotron.createEnhancer()));

render(
    <Provider store={store}>
        <MuiThemeProvider theme={theme}>
            <Router>
                <Switch>
                    <Layout exact path='/' component={App} />
                    {/*<Layout exact path='/login' component={Login}/>*/}
                </Switch>
            </Router>
        </MuiThemeProvider>
    </Provider>,
    document.getElementById('app')
);